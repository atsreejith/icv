﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ICV.Models
{
    public class CustomerData
    {
        public int DataId { get; set; }
        [Display(Name = "")]
        public string LegalName { get; set; }
        [Display(Name = "")]
        public string YearEnd { get; set; }
        [Display(Name = "")]
        public bool AreAccountsAudited { get; set; }
        [Display(Name = "")]
        public int LastAuditedYear { get; set; }
        [Display(Name = "")]
        public decimal TurnOverInLastPeriod { get; set; }
        [Display(Name = "")]
        public string AuditedBy { get; set; }
        [Display(Name = "")]
        public string RecordsMaintainedAt { get; set; }

        [Display(Name = "")]
        public bool AreSeperateBookMaintained { get; set; }
        [Display(Name = "")]
        public bool FSAvailableForLegalEntity { get; set; }
        [Display(Name = "")]
        public string NameOfAccountingSystem { get; set; }
        [Display(Name = "")]
        public bool IsManufacturer { get; set; }
        [Display(Name = "")]
        public decimal ApproxMfgCost { get; set; }
        [Display(Name = "")]
        public bool IsServiceProviderWithGoods { get; set; }
        [Display(Name = "")]
        public string TypeOfGoodsAndValueProcured { get; set; }
        [Display(Name = "")]
        public int NoOfVendors { get; set; }
        [Display(Name = "")]
        public int NoOfVendorsWithICV { get; set; }

        [Display(Name = "Are you a service provider with sub contractor?")]
        public bool IsServiceProviderWithSubContrator { get; set; }

        [Display(Name = "Number of sub-contractors")]
        public int NoOfSubContrators { get; set; }
        [Display(Name = "Do your sub-contractors have an ICV?")]
        public bool SubConHaveICV { get; set; }

        [Display(Name = "Are you a pure service provider?")]
        public bool IsPureServiceProvider { get; set; }
        [Display(Name = "Do you have operations outside UAE?")]
        public bool OperationsOutSideUAE { get; set; }
        [Display(Name = "What is the total value of your investment in UAE?")]
        public decimal TotalValueOfInvestment { get; set; }
        [Display(Name = "Do you have Emarati Employee?")]
        public bool EmaratiEmployee { get; set; }
        [Display(Name = "How many?")]
        public int NoOfEmaratiEmployees { get; set; }

        [Display(Name = "Number of expat employees")]
        public int NoOfExpatEmployees { get; set; }


    }
}
